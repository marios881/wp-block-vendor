<?php
/*
Plugin Name: Block Vendor Directory and Files
Description: A WordPress plugin that blocks access to the vendor directory and specific files for enhanced security.
Version: 0.2
Author: Marios Kaintatzis
Requires at least: 6.4.2
Tested up to: 6.4.2
Requires PHP: 8.1
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html
GitLab Plugin URI: https://gitlab.com/marios881/wp-block-vendor
*/

// Ensure WordPress is loaded
if (!defined('ABSPATH')) {
    exit;
}

const BLOCK_VENDOR_BLOCK_MARKER = 'Block_Vendor_Rules';

// Hook into the activation event
register_activation_hook(__FILE__, 'block_vendor_activation');
// Hook into the deactivation event
register_deactivation_hook(__FILE__, 'block_vendor_deactivation');

// Custom function to run on plugin activation
function block_vendor_activation(): void {
    // Insert the rules into the .htaccess file using insert_with_markers
    insert_with_markers(ABSPATH . '.htaccess', BLOCK_VENDOR_BLOCK_MARKER, explode(PHP_EOL,
        <<<EOF
        <FilesMatch "\.htaccess|\.htpasswd|wp-config\.php|config\.php|composer\.phar|composer\.json|composer\.lock|\.gitignore|\.htpasswd|\.sample|LICENSE_AFL.txt">
            Order allow,deny
            Deny from all
        </FilesMatch>
        
        # Block vendor folder
        <IfModule mod_rewrite.c>
            RewriteEngine on
            RewriteRule ^vendor/ - [F,L,NC]
            # Block psr4 folder
            RewriteRule ^psr4/ - [F,L,NC]
        </IfModule>
        EOF)
    );
}

// Custom function to run on plugin deactivation
function block_vendor_deactivation(): void {
    // Remove the rules from the .htaccess file using insert_with_markers
    insert_with_markers(ABSPATH . '.htaccess', BLOCK_VENDOR_BLOCK_MARKER, array());
}

// on update re-activate
add_action('upgrader_process_complete', 'block_vendor_activation');